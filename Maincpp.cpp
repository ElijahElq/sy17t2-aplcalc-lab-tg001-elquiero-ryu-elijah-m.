#include<iostream>

using namespace std;

class Rect
{
public:
	int xPos;
	int yPos;
	int width;
	int height;
	Rect(int p_x, int p_y, int p_width, int p_height);

	bool didIntersectRect(Rect p_rectToCheck);
};

class Circle
{
public:
	int xPos;
	int yPos;
	int radius;

	Circle(int c_x, int c_y, int c_radius);

	bool didIntersectCircle(Circle c_circleToCheck);
};

// rectangle constructor
Rect::Rect(int p_x, int p_y, int p_width, int p_height)
{
	xPos = p_x;
	yPos = p_y;
	width = p_width;
	height = p_height;
}

// circle constructor
Circle::Circle(int c_x, int c_y, int c_radius)
{
	xPos = c_x;
	yPos = c_y;
	radius = c_radius;
}

// rectangle did intersect
bool Rect::didIntersectRect(Rect p_rectToCheck)
{
	bool l_result = false;

	bool l_xIntersect = false;
	bool l_yIntersect = false;

	//check if intersects x-axis
	if (xPos >= p_rectToCheck.xPos && xPos <= p_rectToCheck.xPos + p_rectToCheck.width)
	{
		l_xIntersect = true;
	}
	else if (xPos + width >= p_rectToCheck.xPos
		&& xPos + width <= p_rectToCheck.xPos + p_rectToCheck.width)
	{
		l_xIntersect = true;
	}

	//check if intersects y-axis
	if (yPos >= p_rectToCheck.yPos && yPos <= p_rectToCheck.yPos + p_rectToCheck.height)
	{
		l_yIntersect = true;
	}
	else if (yPos + height >= p_rectToCheck.yPos
		&& yPos + height <= p_rectToCheck.yPos + p_rectToCheck.height)
	{
		l_yIntersect = true;
	}

	l_result = l_xIntersect && l_yIntersect;

	return l_result;
}

// circle did intersect
bool Circle::didIntersectCircle(Circle c_circleToCheck)
{
	bool l_result = false;

	bool l_xIntersect = false;

	float a_pyth = abs(xPos - c_circleToCheck.xPos); // x distance
	float b_pyth = abs(yPos - c_circleToCheck.yPos); // y distance
	float c_pyth = sqrt(a_pyth * a_pyth + b_pyth * b_pyth); // pythagorean

	//check if intersects hypotenuse is less than or equal to the radii
	if (c_pyth <= radius + c_circleToCheck.radius)
	{
		l_xIntersect = true;
	}


	l_result = l_xIntersect;


	return l_result;
}


void inputForRectangles(float &A_x, float &A_y, float &A_wid, float &A_hei, float &B_x, float &B_y, float &B_wid, float &B_hei);
void inputForCircles(float &A_x, float &A_y, float &A_radius, float &B_x, float &B_y, float &B_radius);

int main()
{
	int whatShape;

	cout << "Input '1' for rectangles, input '2' for circles --> " ;
	cin >> whatShape;
	
	
	system("cls");

	// if rectangle is selected
	if (whatShape == 1)
	{
		float A_positionX = NULL;
		float A_positionY = NULL;
		float A_width = NULL;
		float A_height = NULL;

		float B_positionX = NULL;
		float B_positionY = NULL;
		float B_width = NULL;
		float B_height = NULL;

		inputForRectangles(A_positionX, A_positionY, A_width, A_height, B_positionX, B_positionY, B_width, B_height);


		Rect rectA = Rect(A_positionX, A_positionY, A_width, A_height);
		Rect rectB = Rect(B_positionX, B_positionY, B_width, B_height);

		if (rectA.didIntersectRect(rectB))
		{
			cout << "\nA and B touchy touchied" << endl << endl;
			system("pause");
			system("cls");
		}
		else
		{
			cout << "\nA and B did not touchy touchy" << endl << endl;
			system("pause");
			system("cls");

		}
	}

	// if circle is selected
	else if (whatShape == 2)
	{
		float A_positionX = NULL;
		float A_positionY = NULL;
		float A_radius = NULL;

		float B_positionX = NULL;
		float B_positionY = NULL;
		float B_radius = NULL;

		inputForCircles(A_positionX, A_positionY, A_radius, B_positionX, B_positionY, B_radius);

		Circle circleA = Circle(A_positionX, A_positionY, A_radius);
		Circle circleB = Circle(B_positionX, B_positionY, B_radius);

		if (circleA.didIntersectCircle(circleB))
		{
			cout << "\nA and B touchy touchied" << endl << endl;

			system("pause");
			system("cls");
		}
		else
		{
			cout << "\nA and B did not touchy touchy" << endl << endl;

			system("pause");
			system("cls");
		}
	
	}

	// if neither selected
	else if (whatShape != 1 || whatShape != 2)
	{
		cout << "Invalid input number. Please try again. " << endl << endl;;

		system("pause");
		system("cls");
	}

	
	return 0;
}

// input for rectangles
void inputForRectangles(float &A_x, float &A_y, float &A_wid, float &A_hei, float &B_x, float &B_y, float &B_wid, float &B_hei)
{

	cout << "Input Rectangle A's X-axis position: ";
	cin >> A_x;

	system("pause");
	system("cls");

	cout << "Input Rectangle A's Y-axis position: ";
	cin >> A_y;

	system("pause");
	system("cls");

	cout << "Input Rectangle A's width: ";
	cin >> A_wid;

	system("pause");
	system("cls");

	cout << "Input Rectangle A's height: ";
	cin >> A_hei;

	system("pause");
	system("cls");

	cout << "Input Rectangle B's X-axis position: ";
	cin >> B_x;

	system("pause");
	system("cls");

	cout << "Input Rectangle B's Y-axis position: ";
	cin >> B_y;

	system("pause");
	system("cls");

	cout << "Input Rectangle B's width: ";
	cin >> B_wid;

	system("pause");
	system("cls");

	cout << "Input Rectangle B's height: ";
	cin >> B_hei;

	system("pause");
	system("cls");

	cout << "Rectangle A: " << "\n Position - (" << A_x << ", " << A_y << ") " << "Width = " << A_wid << "Height = " << A_hei << endl << endl;

	cout << "Rectangle B: " << "\n Position - (" << B_x << ", " << B_y << ") " << " Width = " << B_wid << " Height = " << B_hei << endl << endl;

	system("pause");
	system("cls");
}

// input for circles
void inputForCircles(float &A_x, float &A_y, float &A_radius, float &B_x, float &B_y, float &B_radius)
{

	cout << "Input Circle A's X-axis position: ";
	cin >> A_x;

	system("pause");
	system("cls");

	cout << "Input Circle A's Y-axis position: ";
	cin >> A_y;

	system("pause");
	system("cls");

	cout << "Input Circle A's radius: ";
	cin >> A_radius;

	system("pause");
	system("cls");

	cout << "Input Circle B's X-axis position: ";
	cin >> B_x;

	system("pause");
	system("cls");

	cout << "Input Circle B's Y-axis position: ";
	cin >> B_y;

	system("pause");
	system("cls");

	cout << "Input Circle B's radius: ";
	cin >> B_radius;

	system("pause");
	system("cls");

	cout << "Circle A: " << "\n Position - (" << A_x << ", " << A_y << ") " << " Radius = " << A_radius << endl << endl;

	cout << "Circle B: " << "\n Position - (" << B_x << ", " << B_y << ") " << " Radius = " << B_radius << endl << endl;

	system("pause");
	system("cls");
}

